package es.http.service.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="suministra")//en caso que la tabala sea diferente
public class Suministra {

	//Atributos de entidad suministra
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private int id;
	
	@ManyToOne
    @JoinColumn(name = "pieza_id")
    Proveedores proveedor;
	
    @ManyToOne
    @JoinColumn(name = "proveedor_id")
    Piezas pieza;
	
	@Column(name = "precio")//no hace falta si se llama igual
	private int precio;
	
	//Constructores
	
	public Suministra() {
	
	}

	public Suministra(Proveedores proveedor, Piezas pieza, int precio) {
		this.proveedor = proveedor;
		this.pieza = pieza;
		this.precio = precio;
	}


	//Getters y Setters
	/**
	 * @return the proveedor
	 */
	public Proveedores getProveedor() {
		return proveedor;
	}


	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(Proveedores proveedor) {
		this.proveedor = proveedor;
	}


	/**
	 * @return the pieza
	 */
	public Piezas getPieza() {
		return pieza;
	}


	/**
	 * @param pieza the pieza to set
	 */
	public void setPieza(Piezas pieza) {
		this.pieza = pieza;
	}

	/**
	 * @return the precio
	 */
	public int getPrecio() {
		return precio;
	}


	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}

	//Metodo impresion de datos por consola
	@Override
	public String toString() {
		return "Suministra [proveedor=" + proveedor + ", pieza=" + pieza + ", precio=" + precio + "]";
	}

	
	
	
	
	




		
	
	
	
}
