package es.http.service.service;

import java.util.List;

import es.http.service.dto.Piezas;



public interface IPiezasService {

	//Metodos del CRUD
		public List<Piezas> listarPiezas(); //Listar All 
		
		public Piezas guardarPieza(Piezas pieza);	//Guarda un Pieza CREATE
		
		public Piezas piezaXID(int id); //Leer datos de un Pieza READ
		
		public Piezas actualizarPieza(Piezas pieza); //Actualiza datos del Pieza UPDATE
		
		public void eliminarPieza(int id);// Elimina el Pieza DELETE
	

}


