package es.http.service.service;

import java.util.List;

import es.http.service.dto.Proveedores;

public interface IProveedoresService {

	//Metodos del CRUD
		public List<Proveedores> listarProveedores(); //Listar All 
		
		public Proveedores guardarProveedor(Proveedores proveedor);	//Guarda un Proveedor CREATE
		
		public Proveedores proveedorXID(int id); //Leer datos de un Proveedor READ
		
		public Proveedores actualizarProveedor(Proveedores proveedor); //Actualiza datos del proveedor UPDATE
		
		public void eliminarProveedor(int id);// Elimina el proveedor DELETE
	
}


