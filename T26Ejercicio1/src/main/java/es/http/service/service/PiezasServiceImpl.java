package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IPiezasDAO;
import es.http.service.dto.Piezas;

@Service
public class PiezasServiceImpl implements IPiezasService {
	
	@Autowired
	IPiezasDAO iPiezaDAO;

	@Override
	public List<Piezas> listarPiezas() {
		return iPiezaDAO.findAll();
	}

	@Override
	public Piezas guardarPieza(Piezas pieza) {
		return iPiezaDAO.save(pieza);
	}

	@Override
	public Piezas piezaXID(int id) {
		return iPiezaDAO.findById(id).get();
	}

	@Override
	public Piezas actualizarPieza(Piezas pieza) {
		return iPiezaDAO.save(pieza);
	}

	@Override
	public void eliminarPieza(int id) {
		iPiezaDAO.deleteById(id);
	}

}
