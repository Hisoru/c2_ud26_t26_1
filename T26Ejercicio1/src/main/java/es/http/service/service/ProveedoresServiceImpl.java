package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IProveedoresDAO;
import es.http.service.dto.Proveedores;

@Service
public class ProveedoresServiceImpl implements IProveedoresService {
	
	@Autowired
	IProveedoresDAO iProveedorDAO;

	@Override
	public List<Proveedores> listarProveedores() {
		return iProveedorDAO.findAll();
	}

	@Override
	public Proveedores guardarProveedor(Proveedores proveedor) {
		return iProveedorDAO.save(proveedor);
	}

	@Override
	public Proveedores proveedorXID(int id) {
		return iProveedorDAO.findById(id).get();
	}

	@Override
	public Proveedores actualizarProveedor(Proveedores proveedor) {
		return iProveedorDAO.save(proveedor);
	}

	@Override
	public void eliminarProveedor(int id) {
		iProveedorDAO.deleteById(id);
	}

}
